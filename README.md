# Kitty
In this project I try to achieve artificial stupidity.

Starting very small I'm trying to see if I can make an Artificially Stupid agent that can decide for itself.

So I've looked at human intelligence and I'm trying to replicate it.

Here's a mind map of what I think human intelligence is and how I think humans make decisions:

https://coggle.it/diagram/WHlHDXzaeaEfvUyv/t/human-intelligence


I'm aiming at letting the agent build for itself a decision model based on events happening around it.

So instead of having it mutate across generations (evolution) I'm trying to see if it can adapt without dying.

The code right now is a very simple decision between move or don't move.

moving means living.
not moving means dying.

It seems rule based, and it is at the moment but I'm just starting dead simple.