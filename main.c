#include <stdio.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>

#define SCREEN_WIDTH 1300
#define SCREEN_HEIGHT 1000

#define FOOD_COUNT 15

#define INITIAL_NUTRITION 100000

double
distance(int y2, int y1, int x2, int x1)
{
	int y = y2 - y1;
	y *= y;
	int x = x2 - x1;
	x *= x;
	return sqrt(y + x);
}

/// checks to see if p1 == p2
bool
fuzzy_compare(double p1, double p2)
{
	return fabs(p1 - p2) <=
			0.000000000001 * fmin(fabs(p1), fabs(p2));
}

struct matrix
{
	double* data;
	size_t rows;
	size_t cols;
};

typedef struct matrix matrix;

matrix
matrix_init(size_t rows, size_t cols)
{
	if(rows == 0 || cols == 0)
	{
		matrix m = {0};
		return m;
	}

	matrix m;
	m.data = calloc(rows * cols, sizeof(double));
	m.rows = rows;
	m.cols = cols;
	return m;
}

double*
matrix_at(matrix m, size_t x, size_t y)
{
	return &m.data[m.cols * y + x];
}

void
matrix_print(matrix m)
{
	for(size_t y = 0; y < m.rows; y++)
	{
		for(size_t x = 0; x < m.cols; x++)
		{
			printf("%f ", *matrix_at(m, x, y));
		}
		printf("\n");
	}
}

struct window
{
	SDL_Window* sdl_window;
	SDL_Surface* sdl_surface;
};

typedef struct window window;

struct entity
{
	SDL_Surface* surface;
	SDL_Rect loc;
	bool alive;
};

typedef struct entity entity;

enum action
{
	move_up,
	move_down,
	move_right,
	move_left,
	dont_move
};

typedef enum action action;

struct composite_action
{
	action actions[15];
	size_t size;
	size_t max_cap;
	long score;
};

typedef struct composite_action composite_action;

composite_action
composite_action_init()
{
	composite_action a;
	a.size = 0;
	a.max_cap = 15;
	a.score = 0;
	return a;
}

struct vision
{
	double distances[FOOD_COUNT];
};

typedef struct vision vision;

struct memory
{
	size_t last_nutrition;
	composite_action* last_action;
	vision last_vision;
};

typedef struct memory memory;

struct change
{
	double amount;
	size_t where;
};

typedef struct change change;

struct event
{
	enum kind
	{
		kind_action,
		kind_change
	} kind;

	union
	{
		action a;
		change c;
	};
};

typedef struct event event;

struct relationship
{
	int score;
	size_t size;
	event* events_sequence;
};

typedef struct relationship relationship;

relationship
relationship_init()
{
	relationship r;
	r.score = 0;
	r.size = 0;
	r.events_sequence = NULL;
	return r;
}

struct decision_model
{
	composite_action actions[4];
	size_t actions_size;
	size_t actions_max_cap;
	relationship* relationships;
	matrix m;
	size_t relationships_size;
};

typedef struct decision_model decision_model;

decision_model
decision_model_init()
{
	decision_model dm;
	dm.actions[0] = composite_action_init();
	dm.actions_size = 1;
	dm.actions_max_cap = 4;
	dm.relationships = NULL;
	dm.relationships_size = 0;

	size_t all_events = FOOD_COUNT + 5;
	dm.m = matrix_init(all_events, all_events);
	return dm;
}

struct smart_entity
{
	decision_model dm;
	entity e;
	size_t nutrition;
	memory m;
	vision v;
};

typedef struct smart_entity smart_entity;

static window w;
static entity food[FOOD_COUNT];
static smart_entity agent;

vision
vision_init()
{
	vision v = {0};

	return v;
}

void
vision_update(smart_entity* me, entity* food)
{
	for(size_t i = 0; i < FOOD_COUNT; i++)
	{
		me->v.distances[i] = distance(me->e.loc.y, food[i].loc.y,
									  me->e.loc.x, food[i].loc.x);
	}
}

memory
memory_init()
{
	memory m;
	m.last_nutrition = INITIAL_NUTRITION;
	m.last_action = NULL;
	m.last_vision = vision_init();
	return m;
}

void
memory_update(smart_entity* s, composite_action* a)
{
	s->m.last_nutrition = s->nutrition;
	s->m.last_action = a;
	s->m.last_vision = s->v;
}

entity
entity_init(const char* path)
{
	entity e;
	e.loc.x = 0;
	e.loc.y = 0;
	e.alive = true;
	e.surface = SDL_LoadBMP(path);
	return e;
}

void
update_action_score(composite_action* last_action, int value)
{
	last_action->score += value;
}

change*
detect_changes(smart_entity* s, size_t* changes_size)
{
	double* last_distances = s->m.last_vision.distances;
	double* current_distances = s->v.distances;

	change* changes = malloc(FOOD_COUNT * sizeof(change));
	size_t c_count = 0;

	for(size_t i = 0; i < FOOD_COUNT; i++)
	{
		if(fuzzy_compare(last_distances[i], current_distances[i]))
		{
			continue;
		}

		changes[c_count].where = i;
		changes[c_count].amount = current_distances[i] - last_distances[i];
		c_count++;
	}

	*changes_size = c_count;
	return changes;
}

size_t
action_idx(action a)
{
	return FOOD_COUNT + a;
}

void
build_relationships(smart_entity* s)
{
//	size_t changes_size;
//	change* changes = detect_changes(s, &changes_size);

//	size_t a_idx = action_idx(s->m.last_action);
//	matrix relations = s->dm.m;

//	for(size_t i = 0; i < changes_size; i++)
//	{
//		for(size_t j = 0; j < changes_size; j++)
//		{
//			*matrix_at(relations,
//					   changes[i].where,
//					   changes[j].where) += changes[i].amount;
//		}
//	}

//	for(size_t i = 0; i < changes_size; i++)
//	{
//		*matrix_at(relations,
//				   a_idx,
//				   changes[i].where) += changes[i].amount;

//		*matrix_at(relations,
//				   changes[i].where,
//				   a_idx) += -changes[i].amount;
//	}

//	free(changes);

//	matrix_print(relations);
//	printf("\n\n");
}

size_t
least_score_c_action(smart_entity* s)
{
	long least = 999999999;
	size_t i = 0;

	for(; i < s->dm.actions_size; i++)
	{
		if(s->dm.actions[i].score < least)
		{
			least = s->dm.actions[i].score;
		}
	}

	return i - 1;
}

void
create_new_composite_action(smart_entity* s)
{
	size_t idx;

	if(s->dm.actions_size >= s->dm.actions_max_cap)
	{
		idx = least_score_c_action(s);
	}
	else
	{
		idx = s->dm.actions_size;
		s->dm.actions_size++;
	}

	composite_action a = composite_action_init();
	a.size = (size_t)rand() % (a.max_cap) + 1;
	a.score = 0;

	for(size_t i = 0; i < a.size; i++)
	{
		a.actions[i] = rand() % 4;
	}

	s->dm.actions[idx] = a;
}

void
decision_model_update(smart_entity* s)
{
	build_relationships(s);

	if(s->nutrition > s->m.last_nutrition)
	{
		update_action_score(s->m.last_action, 100);
	}
	else
	{
		update_action_score(s->m.last_action, -1);
		create_new_composite_action(s);
	}
}

void
distances_update(smart_entity* s)
{
	for(size_t i = 0; i < FOOD_COUNT; i++)
	{
		s->v.distances[i] =
				distance(s->e.loc.y, food[i].loc.y, s->e.loc.x, food[i].loc.x);
	}
}

smart_entity
smart_entity_init(const char* path)
{
	smart_entity s;
	s.e = entity_init(path);
	s.nutrition = INITIAL_NUTRITION;
	s.dm = decision_model_init();
	s.m = memory_init();
	s.v = vision_init();
	distances_update(&s);
	return s;
}


void
entity_draw(window w, entity e)
{
	SDL_BlitSurface( e.surface, NULL, w.sdl_surface, &e.loc );
}

void
entity_move_x(entity* e, int x)
{
	if(e->loc.x + x < 0)
	{
		return;
	}

	if(e->loc.x + x > SCREEN_WIDTH - 100)
	{
		return;
	}

	e->loc.x += x;
}

void
entity_move_y(entity* e, int y)
{
	if(e->loc.y + y < 0)
	{
		return;
	}

	if(e->loc.y + y > SCREEN_HEIGHT - 100)
	{
		return;
	}

	e->loc.y += y;
}

bool
entity_collide(entity e1, entity e2)
{
	if(!e1.alive || !e2.alive)
	{
		return false;
	}

	double d = distance(e2.loc.y, e1.loc.y, e2.loc.x, e1.loc.x);

	if(d < 100)
	{
		return true;
	}

	return false;
}

void
window_clear(window* w)
{
	SDL_FillRect( w->sdl_surface, NULL,
				  SDL_MapRGB( w->sdl_surface->format, 255, 255, 255 ) );
}

window
window_init()
{
	window w;

	w.sdl_window = SDL_CreateWindow( "Artificial Stupidity",
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );

	w.sdl_surface = SDL_GetWindowSurface( w.sdl_window );

	window_clear(&w);

	return w;
}

void
window_destroy(window w)
{
	SDL_DestroyWindow( w.sdl_window );
	SDL_Quit();
}

composite_action*
entity_decide_motion(smart_entity* s)
{
	long best_score = -9999999;
	composite_action* a = NULL;

	for(size_t i = 0; i < s->dm.actions_size; i++)
	{
		if(s->dm.actions[i].score > best_score)
		{
			a = &s->dm.actions[i];
			best_score = s->dm.actions[i].score;
		}
	}

	return a;
}

void
feed_agent(smart_entity* s)
{
	s->nutrition += 100;
}

void
entity_move(entity* e, composite_action* a, double delta)
{
	int step = (int)delta*3;

	/// \todo split this loop to be a single action executed per update
	for(size_t i = 0; i < a->size; i++)
	{
		if(a->actions[i] == move_up)
		{
			entity_move_y(e, -step);
		}
		else if(a->actions[i] == move_down)
		{
			entity_move_y(e, step);
		}
		else if(a->actions[i] == move_right)
		{
			entity_move_x(e, step);
		}
		else if(a->actions[i] == move_left)
		{
			entity_move_x(e, -step);
		}
	}
}

void
update(double delta)
{
	if(!agent.e.alive)
	{
		return;
	}

	composite_action* a = entity_decide_motion(&agent);
	agent.m.last_action = a;

	distances_update(&agent);
	decision_model_update(&agent);

	entity_move(&agent.e, a, delta);

	memory_update(&agent, a);

	if(agent.nutrition == 0)
	{
		agent.e.alive = false;
	}

	entity_draw(w, agent.e);

	agent.nutrition--;

	for(size_t i = 0; i < FOOD_COUNT; i++)
	{
		if(entity_collide(agent.e, food[i]))
		{
			food[i].alive = false;
			feed_agent(&agent);
		}

		if(food[i].alive)
		{
			entity_draw(w, food[i]);
		}
	}

//	printf("nutrition: %zu\n", agent.nutrition);
//	printf("move_up: %d\n", agent.dm.move_up_score);
//	printf("move_down: %d\n", agent.dm.move_down_score);
//	printf("move_right: %d\n", agent.dm.move_right_score);
//	printf("move_left: %d\n", agent.dm.move_left_score);
//	printf("dontmove: %d\n\n", agent.dm.dont_move_score);
}

void
destroy()
{
	SDL_FreeSurface(agent.e.surface);

	for(size_t i = 0; i < FOOD_COUNT; i++)
	{
		SDL_FreeSurface(food[i].surface);
	}
}

int main()
{
	w = window_init();
	srand(23);

	agent = smart_entity_init(CODE_LOCATION "agent.bmp");

	for(size_t i = 0; i < FOOD_COUNT; i++)
	{
		 food[i] = entity_init(CODE_LOCATION "food.bmp");
		 food[i].loc.x += rand() % SCREEN_WIDTH;
		 food[i].loc.y += rand() % SCREEN_HEIGHT;
	}

	Uint64 then = SDL_GetPerformanceCounter();

	while(true)
	{
		window_clear(&w);

		Uint64 now = SDL_GetPerformanceCounter();
		double delta = (double)((now - then) * 1000
								/ (double)SDL_GetPerformanceFrequency());
		update(delta);
		then = now;

		SDL_UpdateWindowSurface( w.sdl_window );
	}

	return 0;
}
