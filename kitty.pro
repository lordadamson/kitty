TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c

INCLUDEPATH += /usr/include/SDL2

DEFINES += _REENTRANT
DEFINES += CODE_LOCATION="\\\"$$PWD/\\\""

LIBS += -lSDL2
